import React from 'react'
import { Create, SimpleForm, DateInput } from 'react-admin'

const PostCreate = (props) => {
  return (
    <Create title='Select a Date' {...props}>
      <SimpleForm>
        <DateInput label='Published' source='publishedAt' />
      </SimpleForm>
    </Create>
  )
}

export default PostCreate
